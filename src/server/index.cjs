const csvtojson = require('csvtojson');
var fs = require('fs');
let findMatchesPerCity = require('./1-matches-per-city.cjs');
let findMatchesWonPerTeam = require('./2-matches-won-per-team.cjs');
let findRedCardsPerTeamInAYear = require('./3-red-cards-per-team-in-a-year.cjs');
let findTopTenPlayers = require('./4-topTenPlayers.cjs');

csvtojson()
    .fromFile('../data/WorldCupMatches.csv')
    .then((json) => {
        let worldCupMatches = json;

        //Problem 1 : Find number of matches played per city.
        let output1 = findMatchesPerCity(worldCupMatches);
        fs.writeFile('../public/output/1-matches-per-city.json', JSON.stringify(output1), (err) => {
            if (err) {
                console.error(err);
                return;
            }
        });

        //Problem 2 : Find number of matches won per team.
        let output2 = findMatchesWonPerTeam(worldCupMatches);
        fs.writeFile('../public/output/2-matches-won-per-team.json', JSON.stringify(output2), (err) => {
            if (err) {
                console.error(err);
                return;
            }
        });

        //Problem 3 : Find the number of red cards issued per team in 2014 World Cup.
        csvtojson()
            .fromFile('../data/WorldCupPlayers.csv')
            .then((json) => {
                let worldCupPlayers = json;
                let year = 2014;
                let output3 = findRedCardsPerTeamInAYear(worldCupMatches, worldCupPlayers, year);
                fs.writeFile('../public/output/3-red-cards-per-team-in-2014.json', JSON.stringify(output3), (err) => {
                    if (err) {
                        console.error(err);
                        return;
                    }
                });

                //Problem 4 : Find the top 10 players with the highest probability of scoring a goal in a match.
                let output4 = findTopTenPlayers(worldCupPlayers);
                fs.writeFile('../public/output/4-topTenPlayers.json', JSON.stringify(output4), (err) => {
                    if (err) {
                        console.error(err);
                        return;
                    }
                });
            });
    });







