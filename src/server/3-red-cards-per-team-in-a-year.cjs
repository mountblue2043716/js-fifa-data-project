//Problem 3 : Find the number of red cards issued per team in 2014 World Cup.
function findRedCardsPerTeamInAYear(matches, players, year) {
    let matchIDsInSpecifiedYear = matches.filter(match => parseInt(match['Year']) === year)
                            .map(match => match['MatchID']);
    let redCardsPerTeam = players.reduce((redCards, player) => {
        if (player['Event'].includes('R') && matchIDsInSpecifiedYear.includes(player['MatchID'])) {
            redCards[player['Team Initials']] = (redCards[player['Team Initials']] || 0) + 1;
        }
        return redCards;
    },{});
    let redCardsPerTeamName = {}
    for (let teamInitial in redCardsPerTeam) {
        for (let match of matches) {
            if (match['Home Team Initials'] === teamInitial && !redCardsPerTeamName[match['Home Team Initials']]) {
                redCardsPerTeamName[match['Home Team Name']] = redCardsPerTeam[teamInitial];
            }
        }
    }
    return redCardsPerTeamName;
}
module.exports = findRedCardsPerTeamInAYear;



