//Problem 2 : Find number of matches won per team.
function findMatchesWonPerTeam(worldCupMatches) {
    let result = worldCupMatches.reduce((matchesWonPerTeam, match) => {
        if (match['Home Team Goals'] > match['Away Team Goals']) {
            matchesWonPerTeam[match['Home Team Name']] = (matchesWonPerTeam[match['Home Team Name']] || 0) + 1;
        } else if (match['Home Team Goals'] < match['Away Team Goals']) {
            matchesWonPerTeam[match['Away Team Name']] = (matchesWonPerTeam[match['Away Team Name']] || 0) + 1;
        } 
        return matchesWonPerTeam;
    },{});
    return result;
}
module.exports = findMatchesWonPerTeam;
