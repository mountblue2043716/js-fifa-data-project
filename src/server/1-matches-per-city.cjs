//Problem 1 : Find number of matches played per city.
function findMatchesPerCity(worldCupMatches) {
    return worldCupMatches.reduce((matchesPerCity, match) => {
        if (match.City !== '') {
            matchesPerCity[match.City] = (matchesPerCity[match.City] || 0) + 1;
        }
        return matchesPerCity;
    }, {});
}
module.exports = findMatchesPerCity;

