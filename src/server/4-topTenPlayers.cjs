//Problem 4 : Find the top 10 players with the highest probability of scoring a goal in a match.
function findTopTenPlayers(players) {
    let output = players.reduce((playerGoals, player) => {
        let ownGoals = (player['Event'].match(new RegExp('OG', "g")) || []).length;
        let goals = (player['Event'].match(new RegExp('G', "g")) || []).length - ownGoals;
        if (goals > 0) {
            if (playerGoals[player['Player Name']]) {
                playerGoals[player['Player Name']] += goals;
            } else {
                playerGoals[player['Player Name']] = goals;
            }
        }
        return playerGoals;
    },{});

    const sortedObj = Object.fromEntries(
        Object.entries(output).sort((a, b) => b[1] - a[1])
    );
    let topPlayers = Object.keys(sortedObj).slice(0, 10);
    return topPlayers;
}
module.exports = findTopTenPlayers;

